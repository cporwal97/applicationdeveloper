public class ExtentionController {

    public String getCust() {
        return null;
    }

     public Customer1__c objcustomer1 {get;set;}
     public NewOrder1__c objOrder1 {get;set;}
     public String tempId{get;set;}
     List<Customer1__c> cust = new List<Customer1__c>();
     List<NewOrder1__c> ord = new List<NewOrder1__c>();
   //Costructor 
    public ExtentionController(){
     objcustomer1 = new Customer1__c();
     objOrder1 = new  NewOrder1__c(); 
        
     tempId = ApexPages.currentPage().getParameters().get('Id'); 
       if(tempId != null)        
        {
             objcustomer1 = [SELECT Name__c,Email__c,Phone__c,State__c,Street1__c,Street2__c,City__c,Country__c
                               FROM Customer1__c where Id=:tempId limit 1];
            objOrder1 = [Select Product__c,Price__c,Description__c,Quantity__c from NewOrder1__c where Customer1__c=:tempId limit 1 ];
        }
         
     
    }
    public PageReference save()
    {
      //Insert cust;
       // String id1 ; 
        Insert objcustomer1;
         objOrder1.Customer1__c = objcustomer1.id;
         Insert objOrder1;
        PageReference pageRef = new PageReference('/apex/OrderDetails?id='+objcustomer1.Id);
        pageRef.setRedirect(true);
         return pageRef;
    }
}