global class BatchForAccountUpdate implements Database.Batchable<sObject> {
    global final String query;
    global BatchForAccountUpdate(){
        query = 'SELECT id,(Select Id from Contacts) from Account where id in(select AccountId FROM Contact)';
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> scope){
		Set<id> accIDs = new Set<id>();
        Map<Id,List<Contact>> AccountToContact = new Map<Id,List<Contact>>();
        for(Account acc : scope){
            AccountToContact.put(acc.id, acc.Contacts);
            accIDs.add(acc.id);
        }
        
        Map<id,List<Employment__c>> ContactToEmployment = new  Map<id,List<Employment__c>>();
        List<Contact> conList  = [SELECT id,(Select id,Name,Available__c from Employment__r)
                                  from Contact where Id in(Select Contact__C from Employment__C) ];
        for(Contact c : conList ){
            ContactToEmployment.put(c.id,c.Employment__r);
        }
        
        Map<id,Map<id,List<Employment__c>>> ACEMap = new  Map<id,Map<id,List<Employment__c>>>();
        
        for(id accid2 : accIDs ){
            for(Contact con : AccountToContact.get(accid2)){
                if(ContactToEmployment.containsKey(con.Id)){
                    Map<id,List<Employment__c>> temp = new Map<id,List<Employment__c>>();
                    temp.put(con.Id, ContactToEmployment.get(con.Id));
                    ACEMap.put(accid2, temp);
                }
            }
        }
        system.debug('Account COntact and Emloyment records  ==>  ' + ACEMap);
        
      
        
        
        //system.debug('scope' + scope);
        
        
        
        /* map<id,id> accconId  = new map<id,id>();
for(List<Contact> con  : accountConList.values() ){
//accconId.put(con.ID,con.AccountID);

}*/
        
        
        
    }
    global void finish(Database.BatchableContext bc){
        
    }
    
}