public class ContactSearch {
	@AuraEnabled
    public static List<Contact> getContacts(String searchKeyword){
        String skey = searchKeyword + '%';
        system.debug('searchKeyword-->'+ searchKeyword);
        List<Contact> conList = new List<Contact>();
        String query = 'Select Id,FirstName,LastName,Account.Name,Phone from Contact where Name Like:skey';
        conList = Database.query(query);
        system.debug(conList.size());
        return conList;
    }
}