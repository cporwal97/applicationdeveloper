public class AccountController {
 
      @AuraEnabled
      public static List <Account> getAccounts() {
        return [SELECT Id, name, industry, Type, Phone FROM Account ORDER LIMIT 100];
      }
    
}