public class TestController {
    
    public Account acc {get;set;}
    public Contact con {get;set;}
    
    public TestController(){
        acc = new Account();
        con = new Contact();
    }
    public PageReference save(){
        insert acc;
        con.AccountId = acc.Id;
        insert con;
        return null;
    }
    public PageReference cancel(){
        PageReference pg = new PageReference('/001/o');
        pg.setRedirect(true);
        return pg;
    }
 
}