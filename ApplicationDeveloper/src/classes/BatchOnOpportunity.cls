global class BatchOnOpportunity implements Database.Batchable<sObject> {
    global BatchOnOpportunity(){
        
    }
    global Database.QueryLocator start(Database.BatchableContext context){
        return Database.getQueryLocator('SELECT OpportunityId, StageName FROM OpportunityHistory where CreatedDate < LAST_WEEK  ');
    }
    global void execute(Database.BatchableContext context, List<OpportunityHistory> scope){
        integer count = 0;
        Set<Id> oppId = new Set<Id>();
        for(OpportunityHistory opph : scope){
            if(opph.StageName != null){
                oppId.add(opph.OpportunityId);
            }
        }
        List<Opportunity> oppList = [Select Id,StageName from Opportunity where id in : oppId];
        for(Opportunity opp : oppList){
            opp.StageName = 'Closed Lost';
            count++;
        }
        update oppList;
        system.debug('count' + count);
    }
    global void finish(Database.BatchableContext contect){
        
    }
}