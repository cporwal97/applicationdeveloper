@isTest
public class TestOppt {
    public static testMethod void test(){
        //Create Acount
        
        Account acc = new Account(Name = 'IBM');
        insert acc;
        
        //Create Contacts on IBM Account
        List<Contact> clist = new  List<Contact>();
        for(integer i =0; i<100; i++){
            Contact con = new Contact(FirstName = 'Con',
                                      LastName = 'Last' + i ,
                                      AccountId = acc.Id
                                     );
            clist.add(con);
        }
        insert clist;
        
        //Create Opportunity
        Date cdate = system.today() + 14;
        String opstage = 'Prospecting';
        Opportunity op = new Opportunity(Name = 'oppTest1',
                                        AccountId = acc.id,
                                         CloseDate = cdate,
                                         StageName = opstage,
                                         Description = 'New Description is Updated'
                                       );
        
        insert op;
        
       
        
    }
}