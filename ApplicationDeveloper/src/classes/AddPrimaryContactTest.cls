@isTest
public class AddPrimaryContactTest {
    
    
	@testSetup
    static void setup(){
        List<Account> accList = new List<Account>();
        for(Integer i=0; i<50; i++){
            accList.add(new Account(BillingState = 'NY', name = 'QueueTest ' + i));
        }
        for(Integer j=0; j<50; j++){
            accList.add(new Account(BillingState = 'CA', name = 'QueueTest ' + j));
        }        
        insert accList;
        
    }
    @isTest
    static void testQueuable(){
        Contact con = new Contact();
        con.FirstName='demo';
        con.LastName = 'demo';
        String state = 'CA';
        AddPrimaryContact apc = new AddPrimaryContact(con,state);
        test.startTest();
        system.enqueueJob(apc);
        test.stopTest();
        System.assertEquals(50, [select count() from Contact where accountID IN (SELECT id FROM Account WHERE BillingState = :state)]); 
    }
    
    
}