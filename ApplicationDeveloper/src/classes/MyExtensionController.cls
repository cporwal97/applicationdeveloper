public with sharing class MyExtensionController {
public Boolean render1 {get;set;} 
public Boolean render2 {get;set;} 
public Boolean render3 {get;set;} 
public Boolean render4 {get;set;} 
public static integer flag {get;set;} 
   
     public Customer1__c objcustomer1 {get;set;}
     public NewOrder1__c objOrder1 {get;set;}
     public String tempId{get;set;}
     List<Customer1__c> cust = new List<Customer1__c>();
     List<NewOrder1__c> ord = new List<NewOrder1__c>();
    
   //Costructor 
    public MyExtensionController (){
    render1 = true;
    render2 = false;
    render3 = false;
    render4 = false;
    flag = 1;
     objcustomer1 = new Customer1__c();
     objOrder1 = new  NewOrder1__c(); 
        
     tempId = ApexPages.currentPage().getParameters().get('Id'); 
       if(tempId != null)        
        {
             objcustomer1 = [SELECT Name__c,Email__c,Phone__c,State__c,Street1__c,Street2__c,City__c,Country__c
                               FROM Customer1__c where Id=:tempId limit 1];
            objOrder1 = [Select Product__c,Price__c,Description__c,Quantity__c from NewOrder1__c where Customer1__c=:tempId limit 1 ];
        }
         
     
    }
    
    public void save1()
    {

       Insert objcustomer1;
       render1 = false;
       render2 = true;
       render3 = true;
       
       
    }
    public void save2()
    {

       objOrder1.Customer1__c = objcustomer1.id;
       ord.add(objOrder1);
       Upsert ord;
      render2 = false;
      render3 = true;
      render4 = true;
       
       
    }
    public void SaveAndNew(){
        if(flag == 1)
        {
      objOrder1.Customer1__c = objcustomer1.id;
            insert objOrder1;
            ord.add(objOrder1);
            flag++;
    }
       
        else{ 
    objOrder1 = new NewOrder1__c();
    objOrder1 = [Select Product__c,Price__c,Description__c,Quantity__c from NewOrder1__c where Customer1__c=:tempId limit 1 ];
    objOrder1.Customer1__c = objcustomer1.id;
    ord.add(objOrder1);
    upsert ord;
    render2 = true;
        }
     
    }
   
}