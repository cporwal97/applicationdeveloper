@isTest
public class DailyLeadProcessorTest {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	@testSetup
    static void setup(){
        List<Lead> ld = new List<Lead>();
        for(integer i = 0 ; i< 200; i++){
            Lead l = new Lead(LastName = 'testing' + i , Company = 'VoteForDream');
            ld.add(l);
        }
        insert ld;
    }
    @istest
    static void testSchedulable(){
        DailyLeadProcessor updater = new DailyLeadProcessor();
     
        test.startTest();
           system.schedule('LeadUpdater', CRON_EXP, updater);
        test.stopTest();
        List<Lead> countLead = [Select id,LastName,LeadSource from Lead where  LastName LIKE 'testing%' and LeadSource = 'Dreamforce'  ];
        system.debug( countLead.size());
        system.assertEquals(200, countLead.size());
    }
}