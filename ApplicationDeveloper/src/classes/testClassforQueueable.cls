@isTest
public class testClassforQueueable {
    
    @testSetup
    static void setup(){
        List<Contact> conList = new List<Contact>();
        for(integer i = 0 ;i<100 ; i++){
            conList.add(new Contact(LastName = 'testName' + i));
        }
    }
    
    @isTest
    static void createEmail(){
        testQueuableClass updater = new testQueuableClass();
        Test.startTest();  
        //System.enqueueJob(updater);
        testQueuableClass first = new testQueuableClass();
        first.execute(null);
        //Asserts go here
        testQueuableClass2 second = new testQueuableClass2();
        second.execute(null);
        //Asserts go here
        Test.stopTest();        
        // Validate the job ran. Check if record have correct parentId now
        System.assertEquals(0, [select count() from Contact where Email = null]);
        
    }
    
    
    
}