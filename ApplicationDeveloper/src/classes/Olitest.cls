@isTest
public class Olitest {
	
    @isTest
    public static void testoli(){
        Customer__c customer = new Customer__c();
        customer.Name__c = 'Vishal';
        insert customer;
       
        Order1__c ord = new Order1__c();
        ord.Customer__c = customer.id;
        insert ord;

        
         
        Product1__c p = new Product1__c();
        p.Name__c = 'Philips';
        p.Price__c = 1500;
        insert p;
        
        OrderLineItem__c ol = new OrderLineItem__c();
        ol.Order1__c = ord.id;
        ol.Quantity__c = 4;
        ol.Product1__c = p.id;
	test.startTest();
        insert ol;
     test.stopTest();  
        OrderLineItem__c ol1 = [Select id,Approved__c,Status__c,Product_Name__c from OrderLineItem__c where id=: ol.Id];
        system.assertEquals('In Process', ol1.Status__c);
        system.assert(ol1.Approved__c);
        system.assertEquals(ol1.Product_Name__c, p.Name__c);
        
        
    }
   
}