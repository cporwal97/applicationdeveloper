public class SampleTryCatch {
    
    public Lead objLead;
    public String lastName;
    public SampleTryCatch() {
        
    }
    public PageReference newLead() {
        objLead = new Lead(Company = 'TheBlogReaders', LastName = lastName, Status = 'Open');
        try {
            insert objLead;
            PageReference pg = new PageReference('/' + objLead.Id);
            pg.setRedirect(true);
            return pg;
        } catch(DMLException e) {
            return null;
        }
    }
    
}