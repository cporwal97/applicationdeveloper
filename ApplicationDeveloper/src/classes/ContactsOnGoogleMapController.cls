public class ContactsOnGoogleMapController {
public String contactJson{get;set;}
     
    public ContactsOnGoogleMapController(){
         
        List<Contact> conList = [select Id,Name,Email,Phone,MailingLatitude,MailingLongitude,MailingStreet,MailingCity,MailingState,MailingCountry,PhotoUrl from Contact Order By CreatedDate DESC LIMIT 5];
         
        String baseURL = 'https://ap2.salesforce.com';
        String coma = '';
         
        if(conList.size() > 0){
            contactJson = '[';
             
            for(Contact con : conList){
                contactJson += coma + '{\"title\":\"' + con.Name + '\",'+
                                '\"lat\": \"' + con.MailingLatitude + '\",'+
                                '\"lng": \"' + con.MailingLongitude +'\",'+
                                '\"address\": \"' + con.MailingStreet +' '+ con.MailingCity +'<br/>'+ con.MailingState + '<br/>' +con.MailingCountry + '\",'+
                                '\"contact\": \"' + con.Phone + '\",'+
                                '\"icon\": \"' + baseURL +''+ con.PhotoUrl + '\"}';
                                 
                coma = ',';
            }
             
            contactJson += ']';
             
        }
    }
}