public with sharing class CarSearchController {
    @AuraEnabled
    public static List<Car__c> getCars(String carTypeId){
        if(carTypeId != null && carTypeId.equalsIgnoreCase('')){
            return [SELECT Id, Name, Picture__c, Contact__r.Name, Geolocation__Latitude__s, Geolocation__Longitude__s 
                    FROM Car__c
                    where Available_For_Rent__c = true];
        }
        else{
            return [SELECT Id, Name, Picture__c, Contact__r.Name, Geolocation__Latitude__s, Geolocation__Longitude__s 
                    FROM Car__c 
                    where Car_Type__c =: carTypeId
                    AND Available_For_Rent__c = true];
        }
    }
}