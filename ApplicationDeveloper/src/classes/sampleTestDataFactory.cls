@isTest
public class sampleTestDataFactory {

  public static  Account accountRecord()
    {
        Account acc = new Account(name='TestDataAccount',BillingState='CA');
        return acc;
    }
   public static Contact contactRecord(Id accId)
    {
        Contact con = new Contact(AccountId=accId, FirstName='Chitransh', LastName='Porwal');
        return con;
    }
}