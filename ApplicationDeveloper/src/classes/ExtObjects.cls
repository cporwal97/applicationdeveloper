public class ExtObjects {
    public List<Account> acct{get;set;}
    public List<Contact> con{get;set;}
    public Id eventId{get;set;}
    public ExtObjects(ApexPages.StandardController controller) {
		acct = new List<Account>();
        con = new List<Contact>();
    }
    public void showAccount(){
        acct = [select Id,Name from Account limit 10];
    }
    public void showContacts(){
        System.debug('id++++++++++++' + eventId);
        con = [Select LastName,FirstName,Account.Name from Contact where AccountId =:eventId];
    }
}