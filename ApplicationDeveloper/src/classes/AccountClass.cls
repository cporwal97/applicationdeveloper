public class AccountClass {

    public static void afterInsert(List<Account> accountToCreate)
    {
         List<Contact> contactToCreate = new List<Contact>();
        for(Account acc : accountToCreate)
        {
            Contact con = new Contact();
            con.FirstName = 'Contact created from Trigger';
            con.LastName = 'Last Name';
            con.AccountId = acc.Id;
           contactToCreate.add(con);
        }
        insert contactToCreate;
    }
    public static void beforeInsert(List<Account> accountToCreate)
    {
        for(Account acc : accountToCreate)
        {
            Account existingAccount = acc;
            existingAccount.Name = existingAccount.Name + ' - ' + 'Updated';
        }
    }
    
    public static void afterUpdate(List<Account> accountToCreate)
    {
         List<Contact> contacts = [select Id, LastName from
                                 Contact where AccountId IN:accountToCreate];
        for(Contact cont : contacts){
            cont.LastName = 'New Name';
            cont.Department = 'Science';
        }
        update contacts;
    }
}