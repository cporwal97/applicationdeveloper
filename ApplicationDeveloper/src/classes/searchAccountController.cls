public with sharing class searchAccountController {
 
 @AuraEnabled
 public static List < TestCustomer__c > fetchAccount(String searchKeyWord) {
  String searchKey = searchKeyWord + '%';
  List < TestCustomer__c > returnList = new List < TestCustomer__c > ();
  List < TestCustomer__c > lstOfAccount = [select id, Salary__c, Phone__c from TestCustomer__c
                                   where Name LIKE: searchKey LIMIT 500];
 
  for (TestCustomer__c acc: lstOfAccount) {
   returnList.add(acc);
  }
  return returnList;
 }
}