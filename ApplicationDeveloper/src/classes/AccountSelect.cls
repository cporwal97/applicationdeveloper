public class AccountSelect {
    
      public List<wrapAccount> wrapAccountList {get; set;}
     public AccountSelect(){
        if(wrapAccountList == null) {
            wrapAccountList = new List<wrapAccount>();
            for(Account a: [select Id, Name, Phone from Account limit 10]) {
                wrapAccountList.add(new wrapAccount(a));
            }
        }
    }
    
    public PageReference deleteAndRerender(Id delId){
        system.debug(delId);
       	Account ac = [Select id,name from Account where id =: delId];
        delete ac;
        return null;
    }
    
    
    //wrapper
	   public class wrapAccount {
        public Account acc {get; set;}
        public Boolean selected {get; set;}
 
        public wrapAccount(Account a) {
            acc = a;
            selected = false;
        }
    }
    
   
}