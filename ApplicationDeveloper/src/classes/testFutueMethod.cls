global class testFutueMethod {
  
	@future
    public static void createContact(List<id> AccIds){
        List<Contact> conList = new List<Contact>();
        for(integer i = 0; i < AccIds.size(); i++){
            Contact con = new Contact();
        	con.AccountId = AccIds[i];
            con.LastName = 'testFutureContact';
            conList.add(con);
        }
        insert conList;
    }
}