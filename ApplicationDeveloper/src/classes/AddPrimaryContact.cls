public class AddPrimaryContact implements Queueable {
    Contact con = new Contact();
    String State;
   List<Account> accountList = new List<Account>();
     List<Contact> clist = new List<Contact>();
    public AddPrimaryContact(Contact con , String State ){
        	this.con = con;
        this.State = State;
        }
    
    public void execute(QueueableContext context){
        for(Account acc : [Select Id,BillingState,(Select Id,FirstName, LastName from Contacts)  from Account where BillingState =: State limit 200]){
           Contact cc = con.clone(true,false,false,false);
            cc.AccountId = acc.Id;
           clist.add(cc);
           
        }
        
        insert clist;
    }
}