global class OwnerChangeToQueue implements Database.Batchable<Sobject>
{    
    
    
    global  String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC)   
    { 
        query ='SELECT id, name, ownerId,Country,DateSetForStatus__c FROM Lead where StatusCheck__c = true and AssignedUserToCoorectQueue__c=false';
        return Database.getQueryLocator(query);
    } 
    
    global void execute(Database.BatchableContext BC, List<Lead> scope)
    {
       Map<String, TimeSetForCountries__mdt> myMap = new Map<String, TimeSetForCountries__mdt>(); 
        for(TimeSetForCountries__mdt objCS : [Select Label, Active__c, NoOfDays__c from TimeSetForCountries__mdt where Active__c =: true])
            myMap.put(objCS.Label, objCS);
        
        List<Lead> finalList = new List<Lead>();
        for(Lead l :scope)
        {     
          if(myMap.containsKey(l.Country)){
               Id businessHoursId = [select Id from BusinessHours where isdefault = true ].id;
               DateTime now = DateTime.now();
               //DateTime now1 = now.addMinutes(330);
               Integer daysInactive = (Integer)myMap.get(l.Country).NoOfDays__c;
               Long daysInHours = daysInactive*24;
               DateTime value2 = now - daysInactive;
               DateTime value1 = l.DateSetForStatus__c;
              
               system.debug('Value 1' + value1 );
               system.debug('Value 2' + value2 );
              
              /*if(myMap.containsKey(l.Country)){
               if((l.DateSetForStatus__c.minute() <= (system.now().minute() - (Integer)myMap.get(l.Country).NoOfDays__c))){
                    finalList.add(l); 
                }
            }

*/
          Long milliseconds = BusinessHours.diff( businessHoursId, value2, value1 );
          
              Long min = ( milliseconds / 1000 / 60 );
             
            // convert milliseconds into hours
            Long hours = ( milliseconds / 1000 / 60 / 60 );
            system.debug('Hours+++++++++++++++' + hours );  
        
              if(hours <= daysInHours){
                   finalList.add(l); 
              }
            }
        }
        
        Map<String,Id> myMap2 = new  Map<String,Id>(); 
        for(Group abc : [Select Id,Name from Group where Type = 'Queue']){
            myMap2.put( abc.Name,abc.Id);
        }
        
        List<Lead> newList = new List<Lead>();
        
        for(Lead l:finalList)
        {
            string country = l.Country; 
            switch on country{    
                when 'DE'   
                {l.ownerId =myMap2.get('DE_Queue') ;
                 l.AssignedUserToCoorectQueue__c=TRUE;}
                
                when 'FR'    
                {l.ownerId =myMap2.get('FR_Queue') ;
                 l.AssignedUserToCoorectQueue__c=TRUE;}
                
                when 'ES'    
                { l.ownerId =myMap2.get('ES_Queue') ;
                 l.AssignedUserToCoorectQueue__c=TRUE;}
                
                when 'IT'    
                {l.ownerId =myMap2.get('IT_Queue') ;
                 l.AssignedUserToCoorectQueue__c=TRUE;}
                
                when 'AT'    
                { l.ownerId =myMap2.get('AT_Queue') ;
                 l.AssignedUserToCoorectQueue__c=TRUE;}
                
                when 'CH'   
                {l.ownerId =myMap2.get('CH_Queue') ;
                 l.AssignedUserToCoorectQueue__c=TRUE;}
                
                when 'UK'    
                { l.ownerId =myMap2.get('UK_Queue') ;
                 l.AssignedUserToCoorectQueue__c=TRUE;}
                
                when 'US'    
                {l.ownerId =myMap2.get('US_Queue') ;
                 l.AssignedUserToCoorectQueue__c=TRUE;}
                
                when 'BR'    
                { l.ownerId =myMap2.get('BR_Queue') ;
                 l.AssignedUserToCoorectQueue__c=TRUE;}
                
                when 'CZ/SK'    
                {l.ownerId =myMap2.get('CZ/SK_Queue') ;
                 l.AssignedUserToCoorectQueue__c=TRUE;}
                
                when 'DSI'   
                { l.ownerId =myMap2.get('DSI_Queue') ;
                 l.AssignedUserToCoorectQueue__c=TRUE;}
                
                when else {
                    
                    System.debug('default');
                }
                
            }
            newList.add(l);
            
        }
        update newList;    
    }
    global void finish(Database.BatchableContext BC)
    { 
        
    }
}