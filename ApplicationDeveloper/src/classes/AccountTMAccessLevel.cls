global class AccountTMAccessLevel implements Database.Batchable<Sobject> {
    global  String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        Datetime halfHourBack = Datetime.now().addMinutes(-30);
        query ='Select  Id, AccountId,OpportunityAccessLevel From AccountTeamMember WHERE(LastModifiedDate <= :halfHourBack ) ';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<AccountTeamMember> accTeamMembs) {  
        system.debug('List of AccountTeamMember -->>>'+ accTeamMembs);
        
       Set<Id> accIds = new Set<Id>();
		for(AccountTeamMember atm : accTeamMembs){
            accIds.add(atm.AccountId);
        }
		Map<Id, Service__c> serviceMap = new Map<Id, Service__c>([SELECT Id from Service__c WHERE Opportunity__r.AccountId IN: accIds]);
        system.debug('Map of Service -->>>'+ serviceMap);
		List<Service__share> shareServiceList = new List<Service__share>();
        for(Service__c service : serviceMap.values()){
			for(AccountTeamMember atm : accTeamMembs){
				if(service.Opportunity__r.AccountId == atm.AccountId){
					Service__share servShare = new Service__share();
					servShare.ParentId = service.Id;
					servShare.UserOrGroupId = atm.UserId;
					servShare.AccessLevel = atm.OpportunityAccessLevel;
					shareServiceList.add(servShare);
				}
			}
		}
		insert shareServiceList;
    
    }
    global void finish(Database.BatchableContext BC){
        
    }
}