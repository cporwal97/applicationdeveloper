public class ContactsListController {

String myId;

    public ContactsListController(ApexPages.StandardController controller) {
    
    myId=  ApexPages.currentPage().getParameters().get('id');

    }

private String sortOrder = 'LastName';

public List<Contact> getContacts() {

    List<Contact> results = Database.query(

        'SELECT Id, FirstName, LastName, Title, Email ' +

        'FROM Contact ' +
        
        'WHERE AccountId = :myId ' +

        'ORDER BY ' + sortOrder + ' ASC ' +

        'LIMIT 10'

    );

    return results;

}

}