public class Employee {
	private string name;
    private string dept;
    private decimal salary;
    private integer empno;
    public Employee()
    {
        empno = 0;
        name = 'Unknown';
        dept = 'Not Assigned';
        salary = 0;
    }
    
   public Employee(integer eno, string nm,string dep,decimal sal)
    {
       empno = eno;
        name = nm;
        dept = dep;
        salary = sal;
    }
    public string getEmpDate()
    {
        return 'Emp No:' + empno +'Ename:' + name + 'Department:' + dept + 'Salary:' + salary;
        }
}