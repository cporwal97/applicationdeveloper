({
	doInit : function(component, event, helper) {
		helper.onInit(component, event, helper);
	},
    
    onSave : function(component, event, helper) {
        component.set("v.carExperience.Car__c" , component.get("v.car.Id"));
        component.find("service").saveRecord($A.getCallback(function(saveResult){
        if(saveResult.state === "SUCCESS" || saveResult.state ==="DRAFT"){
            var resultsToast = $A.get("e.force:showToast");
            if(resultsToast){
                resultsToast.setParams({
                    "title" : "Saved",
                    "message" : "Car Experience Added"
                });
                resultsToast.fire();
            }
            else{
                alert("Car Experience Added");
            }
            helper.onInit(component, event, helper);
        }
        else if(saveResult.state ==="INCOMPLETED"){
            helper.showToast(component, event, helper,{
                 "title" : "ERROR!",
                    "type" : "error",
                    "message" : "The Record was updated"
            });
        
        }
        else if(saveResult.state ==="ERROR"){
            helper.showToast(component, event, helper,{
                 "title" : "ERROR!",
                    "type" : "error",
                    "message" : "pROBLEM sAVING rECORD"
            });
        
        }
         else {
            helper.showToast(component, event, helper,{
                 "title" : "ERROR!",
                    "type" : "error",
                    "message" : "UNKNOWN PROBLEM"
            });
        
        }
        
    })
   )
    },
    
    onRecordUpdated :function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "CHANGED"){
            var changedFields = eventParams.changedFields;
            helper.showToast(component, event, helper,{
                 "title" : "Saved",
                    "type" : "error",
                    "message" : "The Record was updated"
            });
        }
         else if(eventParams.changeType === "LOADED"){
         }
        else if(eventParams.changeType === "REMOVED"){
         }
        else if(eventParams.changeType === "ERROR"){
         }
    }
})