({
	doFormSubmit : function(component, event, helper) {
		var carTypeId1 = event.getParam('carTypeId');
        console.log('Selected Car Type id' + carTypeId1);
        
        var carSearchResultComponent = component.find("carSearchResult");
        
        //here we are calling the AUra method of CarsearchResult component
        
        var carSearchCmpResult = carSearchResultComponent.searchCars(carTypeId1);
        
        
	}
    
})