({
      doInit: function(component, event, helper) {
        // Fetch the account list from the Apex controller
        helper.getAccountList(component);
      },
     
     viewRecordDetails: function(component, event, helper){
     var account = component.get("v.account");
         var appEvent = $A.get("e.c:ShowAccountDetail");
         if(appEvent){
             appEvent.setParams({
                 "account" : account
             });
             appEvent.fire();
         }
     }
     
    })