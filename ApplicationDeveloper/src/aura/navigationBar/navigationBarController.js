({
    doInit : function(component, event, helper) {
        var menuItems = [
            {
                "label" : "Home",
                "id" : "/"
            },
            {
                "label" : "records",
                "subMenu" : [{
                    "label" : "accounts",
                    "id" : "/accounts"
                    
                },
                {
                    "label" : "contacts",
                    "id" : "/contacts"
                    
                }
                            ]}
            
        ];
        component.set("v.menuItems", menuItems);
    },
    onClick : function(component, event, helper){
        alert(event.target.dataset.menuItemId);
    }
})