({
     
    doInit : function(component, event, helper) {
       // debugger;
        var createCarRecord = $A.get("e.force:createRecord");
         if(createCarRecord){
            component.set("v.showNew",true);
        }
        else{
            component.set("v.showNew",false);
        }
       
        /*var Types = component.get("v.CarTypes");
        component.set("v.CarTypes" , ['Sports Car','Luxury Car', 'Van','Micro','Mini','sadan']);
        Types = component.get("v.CarTypes");*/
        helper.getCarType(component, helper);
    },
    
    onSearchClick : function(component, event, helper) {
     var searchFormSubmit = component.getEvent("searchFormSubmit");
        searchFormSubmit.setParams({
            //get selected Cartypeid from picklist
            //pass in event attribute
            "carTypeId" : component.find("CarTypeList").get("v.value")
        });
        searchFormSubmit.fire();
	},
    
    createRecord : function (component, event, helper) {
    var createCarRecord = $A.get("e.force:createRecord");
    createCarRecord.setParams({
        "entityApiName": "Car_Type__c"
    });
    createCarRecord.fire();
}
    
    /* 
     * 
     * onSearchClick : function(component, event, helper) {
      helper.handleOnSearchClick(component, event, helper);
	},
    
    newValueSelected : function(component, event, helper) {
        var changeVlaue = component.find("CarTypeList").get("v.value");
        
	} ,
    
    toggleClick : function(component, event, helper) {
      var currentValue = component.get("v.isNewAvailable");
        if(currentValue){
            component.set("v.isNewAvailable",false);
        }
        else{
            component.set("v.isNewAvailable",true);
        }
       
	},*/
    
   
    
    
})