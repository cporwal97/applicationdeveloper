({
    Search : function(component, event, helper) {
        var skeyword = component.find("searchId");
        var svalue = skeyword.get("v.value");
        
        
        if (svalue == '' || svalue == null) {
            // display error message if input value is blank or null
            skeyword.set("v.errors", [{
                message: "Enter Search Keyword."
            }]);
        } else {
            skeyword.set("v.errors", null);
            // call helper method
            helper.SearchHelper(component, event);
        }
        
    },
    
    viewDetails : function (component, event, helper) {
        
        var eventId = event.target.getElementsByClassName('con-id')[0].value;
        console.log("eventId" + eventId);
        var navEvt = $A.get("e.force:navigateToSObject");
        
        navEvt.setParams({
            "recordId": eventId,
            "slideDevName":"detail"
        });
        navEvt.fire();
    }
})