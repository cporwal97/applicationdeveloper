trigger CreateOpportunity on Account (after insert, after update) {
    List<Opportunity> opp = new List<Opportunity>();
    try{
    if(trigger.isAfter){
        for(Account acc : trigger.new){
            if(acc.Status__c == 'Completed'){
                Opportunity newOpp = new Opportunity(Name = acc.Name + '\'s' + 'opportunity' , 
                                                    Accountid = acc.Id,
                                                     StageName = 'Needs Analysis',
                                                     CloseDate = Date.today() + 30
                                                    );
                opp.add(newOpp);
            }
        }
        insert opp;
    }
    }
    catch(DMLException e){
        
    }
    
}