trigger onOpportunityTrigger on Opportunity (after insert) {
	List<Opportunity> opplist = [select id,Description from Opportunity where Id in: Trigger.New and Account.Name = 'IBM'];  //and Account.Name = 'IBM'
    List<Account> acc = [Select id,name,(Select id,Description from Contacts) from Account where Name = 'IBM' ];//where Name = 'IBM'
    List<Contact> updateConlist = new  List<Contact>() ;
    for(Opportunity opp : opplist){
      
            for(Account a : acc){
                for(Contact c : a.Contacts){
                    c.Description = opp.Description;
                    updateConlist.add(c);
                }
            }
        
    }
    update updateConlist;
}