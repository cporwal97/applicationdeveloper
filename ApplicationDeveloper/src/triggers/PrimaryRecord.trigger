trigger PrimaryRecord on Contact (before insert, before update) {
  List<Contact> conList = new List<Contact>();
    Map<Id,Contact> accConmap = new map<Id,Contact>();
    Set<Id> accId = new Set<Id>();
    for(contact con: Trigger.new )
        {
        	accId.add(con.AccountId);    
        }
    If(Trigger.IsInsert)
    {
        List<Contact> conUpdate = new List<Contact>();
        conList = [Select id,AccountId,Primary__c from Contact where Primary__c=true and AccountId IN:accId];
        if(conList.Size()>0)
        {
            for(Contact con:conList )
            {
                accConmap.Put(con.AccountId,con);
            }
            for(Contact con: Trigger.New)
            {
                if(con.Primary__c == true)
                {
                    if(accConmap.containskey(con.AccountId))
                    {
                        Contact c= accConmap.get(con.AccountId);
                        c.Primary__c = false;
                        conUpdate.add(c);
                    }
                }
                else
                {
                    if(!accConmap.containskey(con.AccountId))
                    {
                        con.Primary__c = true;
                    }
                }
            }
            if(conUpdate.size()>0)
                update conUpdate;
            
        }
        else{
        for(Contact con:Trigger.new)
            con.Primary__c= true;
        }
    }
   /* //Trigger on Insert
      	List<Contact> newconlist1 = new List<Contact>();
        if(trigger.isInsert){
            List<Contact> co = new list<Contact>();
        	for(Contact c: trigger.new){
                Id a = c.AccountId;
                if(a != null){
                    //fetch the contact of an account which is already primary
                   co = [Select Id,Primary__c from Contact where (Primary__c = true) and (Account.Id =: a )];
                }
               if(c.Primary__c == true && co.size()>0)
                  {
                      for(Contact con : co){
                          con.Primary__c = false;
                          newconlist1.add(con);
                      }
                      
                  
                  }
                else if(c.Primary__c == false && co.size() == 0){
                   c.Primary__c = true;
               }
           }
         update newconlist1;
       }
    List<Contact> newconlist2 = new List<Contact>();
    if(trigger.isUpdate){
        List<Contact> co = new list<Contact>();
    
        for(Contact c: trigger.new){
                Id a = c.AccountId;
                if(a != null){
                   co = [Select Id,Primary__c from Contact where (Primary__c = true) and (Account.Id =: a )];
                }
               
               if(c.Primary__c == true && co.size() > 0 )
                  {
                     
                      for(Contact con : co){
                          con.Primary__c = false;
                          newconlist2.add(con);
                          
                      }
                 
                   update newconlist2;
                  }
            /*if(c.Primary__c == false && co.size() == 1 && flag == 0)
                  {
                     c.lastName.addError('One Primary Contact is Required in a Account');
                  }
               
           }
        
       
    }*/
           
}