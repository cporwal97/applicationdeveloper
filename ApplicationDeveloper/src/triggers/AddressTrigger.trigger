trigger AddressTrigger on Address__c (before insert,before update) {
		
    List<Address__c> billAddList = new List<Address__c>();  

    for(Address__c d : trigger.new){
       if(d.Active__c == true && d.Type__c=='Billing'){
           
             billAddList = [SELECT Customer__c,Active__c FROM Address__c
          WHERE Active__c=true AND Customer__c IN:[select Name from Customer__c]];
           system.debug(billAddList);
 			for(Address__c ba : billAddList){
        			ba.Active__c=false;
            }
        }
    if(d.Active__c == true && d.Type__c=='Shipping'){
           
             billAddList = [SELECT Customer__c,Active__c FROM Address__c
          WHERE Active__c=true AND Customer__c IN:[select Name from Customer__c]];
 			for(Address__c ba : billAddList){
        			ba.Active__c=false;
            }
        }
    }
    update billAddList;
}