trigger test on Lead(before update)
{
    for(Lead ld: trigger.new)
    {
        if(ld.Email!=trigger.oldMap.get(ld.id).email)
        {
            ld.Description=trigger.oldMap.get(ld.id).email;
            update ld;
        }
    }
}