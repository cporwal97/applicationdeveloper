trigger onOpportunityTriggerversion2 on Opportunity (before insert) {
    
   List<Opportunity> opplist = [select id,Description,Account.Name from Opportunity where Id in: Trigger.New ];  
   Set<Id> accids = new Set<Id>();
    for(Opportunity opp1 : opplist){
       accids.add(opp1.AccountId);
    }
    List<Account> acc = [Select id,name,(Select id,Description from Contacts) from Account where Id in: accids ];
    List<Contact> updateConlist = new  List<Contact>() ;
    for(Opportunity opp : opplist){
      
            for(Account a : acc){
                for(Contact c : a.Contacts){
                    c.Description = opp.Description;
                    updateConlist.add(c);
                }
            }
        
    }
    update updateConlist;
}

//// Account acc1 = [Select id,name,(Select id,Description from Contacts) from Account where Id =: opp1.AccountId];
       // acclist.add(acc1);
 // List<Account> acclist = new  List<Account>();