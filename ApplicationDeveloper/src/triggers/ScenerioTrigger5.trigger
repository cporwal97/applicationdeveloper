trigger ScenerioTrigger5 on Lead (before delete) {
	for(Lead l : Trigger.old)
    {
        l.addError('You can not delete a lead ');
    }
}