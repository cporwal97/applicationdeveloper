trigger PriceTrigger on OrderLineItem__c (after insert, after update) {
     Decimal amount = 0.0;
    if(Trigger.isInsert){
       
        try{
            for(OrderLineItem__c item : Trigger.New)
            {
                 Order1__c ord = [SELECT Id, Total_Price__c  FROM Order1__c WHERE Id = :item.Order1__c];
                
                List<OrderLineItem__c> l_co = [SELECT Id, Total_Price__c FROM OrderLineItem__c WHERE Order1__c = :ord.Id];
                   
                for(OrderLineItem__c am_co : l_co) {
                        
                        amount += am_co.Total_Price__c; 
                }
                ord.Total_Price__c = amount;
                update ord;
            }
        }
        catch(Exception e){
            System.debug(e);
        }
    }
    
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            try{
            for(OrderLineItem__c item : Trigger.Old)
            {
                 Order1__c ord = [SELECT Id, Total_Price__c  FROM Order1__c WHERE Id = :item.Order1__c];
                
                List<OrderLineItem__c> l_co = [SELECT Id, Total_Price__c FROM OrderLineItem__c WHERE Order1__c = :ord.Id];
                   
                for(OrderLineItem__c am_co : l_co) {
                        
                        amount += am_co.Total_Price__c; 
                }
                ord.Total_Price__c = amount;
                update ord;
            }
        }
        catch(Exception e){
            System.debug(e);
        }
    }
    }
}