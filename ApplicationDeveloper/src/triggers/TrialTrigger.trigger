trigger TrialTrigger on Account (before insert, after insert, after update) {
    if(Trigger.isAfter && Trigger.isInsert)
    {
       AccountClass.afterInsert(Trigger.new);
    }
    if(trigger.isBefore && trigger.isInsert)
    {
        AccountClass.beforeInsert(Trigger.new);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate){
       
        AccountClass.afterUpdate(Trigger.new);
    }

}