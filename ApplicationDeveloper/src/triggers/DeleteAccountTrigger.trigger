trigger DeleteAccountTrigger on Account (before delete) {
    List<Account> accList = [Select id, (select id from CustomContacts__r) from Account where Id In: Trigger.OldMap.keyset()];
    for(Account acc : accList){
        if(acc.CustomContacts__r.size() > 3){
            Trigger.oldMap.get(acc.Id).addError('Accounts with more than 3 contacts are not available for delete operation');
        }
    }
}