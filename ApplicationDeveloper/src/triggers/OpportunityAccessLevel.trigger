trigger OpportunityAccessLevel on Account (after insert, after update) {
    List<id> accid = new List<id>();
    for(Account a: trigger.new){
        if (a.AccountTeamMembers != null) {
			accid.add(a.Id);
            
    }
 }
    system.debug('List of Accounts' + accid);
    List<AccountTeamMember> atm = new List<AccountTeamMember>();
    List<Opportunity> oppIdList = new List<Opportunity>();
    List<Service__c> sList = new List<Service__c>();
    List<Service__Share> shareList = new List<Service__Share>();
    for (AccountTeamMember acctMembers : [Select  Id, AccountId, OpportunityAccessLevel From AccountTeamMember 
                                          where AccountID in :accid ]){
		if (acctMembers.Id != null) {
 			 //atm.add(acctMembers) ; 
 			oppIdList = [SELECT Id FROM Opportunity where AccountId =:acctMembers.AccountId]; 
            system.debug('List of Opportunity' + oppIdList);
            sList = [SELECT Id, Opportunity__c FROM Service__c where Opportunity__c in :oppIdList];
         //   system.debug('List of Service__c' + sList);
            Service__Share serviceShare = new  Service__Share();
            serviceShare.AccessLevel = acctMembers.OpportunityAccessLevel;
 			system.debug('ServiceAccessLevel -->' + serviceShare.AccessLevel);             
             shareList.add(serviceShare);
             
		}
       //upsert shareList;
    }
    
   
}