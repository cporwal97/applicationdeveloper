trigger OrderLineItemtrigger on OrderLineItem__c (before insert, before update) {
 
   for(OrderLineItem__c oli : trigger.new){
       if(Trigger.isInsert){
           system.debug('in insert');
          if(oli.Email__c == null){
               
           Id orderid = oli.Order1__c;
           Order1__c ord = [Select id,Customer__C from Order1__c where id=: orderid Limit 1];
           Customer__c customer = [Select id,Name__c from Customer__C where id=: ord.Customer__c];
            oli.Email__c = customer.Name__c + '@company.com';
           }
           if(oli.Quantity__c > 2){
               system.debug('in quantity if'+oli.Quantity__c);
               oli.Approved__c = true;
               oli.Status__c = 'In Process';
           }
           
           if(oli.Product1__c == null){
               oli.Product1__c.addError('Please Insert one product');
           }
           else{
               Product1__c prod = [Select id,Name__c from Product1__c where id=: oli.Product1__c];
               oli.Product_Name__c = prod.Name__c;
           }
       }
     if(Trigger.isUpdate){
           if(oli.Email__c == null){
           Id orderid = oli.Order1__c;
           Order1__c ord = [Select id,Customer__C from Order1__c where id=: orderid Limit 1];
           Customer__c customer = [Select id,Name__c from Customer__C where id=: ord.Customer__c];
            oli.Email__c = customer.Name__c + '@company.com';
           }
           if(OLI.Quantity__c > 2){
               oli.Approved__c = true;
               oli.Status__c = 'In Process';
           }
           
           if(oli.Product1__c != null){
               Product1__c prod = [Select id,Name__c from Product1__c where id=: oli.Product1__c];
               oli.Product_Name__c = prod.Name__c;
           }
       }
       
        
     
    }
}