trigger ScenerioTrigger7 on Customer_Project__c (after insert,after update) {
    List<Opportunity> oppList = new List<Opportunity>();
    for(Customer_Project__c cp : Trigger.new){
        if(cp.Status__c == 'Active'){
           
           // Opportunity o1 = new Opportunity(id= cp.Opportunity__c);
            Opportunity o1 = [Select id from Opportunity where id=: cp.Opportunity__c];         
            o1.Active__c = True;
            oppList.add(o1);
        }
        if(cp.Status__c == 'Inactive'){
           // Customer_Project__c c1 = [Select id,Name,Opportunity__r.id from Customer_Project__c];
            Opportunity o1 = new Opportunity(id= cp.Opportunity__c);
            o1.Active__c = False;
            oppList.add(o1);
        }
    }
    update oppList;
}