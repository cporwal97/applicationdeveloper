trigger testFutureMethodTrigger on Account (after insert) {
    List<Id> accIds = new List<Id>();
    for(Account acc: trigger.new){
        accIds.add(acc.Id);
    }
    testFutueMethod.createContact(accIds);
}