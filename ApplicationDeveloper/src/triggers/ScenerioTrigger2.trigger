trigger ScenerioTrigger2 on CustomerTrigger__c (before update) {
 List<TestCustomer__c> list1 = new List<TestCustomer__c>();
      
    for(CustomerTrigger__c c : Trigger.old){
        TestCustomer__c test = new TestCustomer__c();
        test.Name = c.Name;
        test.Salary__c = c.Salary__c;
        test.Phone__c = c.Phone__c;
        list1.add(test);
    }
insert list1;
}